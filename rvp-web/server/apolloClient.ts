import { ApolloClient, HttpLink, InMemoryCache } from "@apollo/client";

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: new HttpLink({
    uri: "https://stage.rvp-api.com/graphql",
    fetchOptions: {
      mode: "cors",
    },
  }),
});

export default client;
