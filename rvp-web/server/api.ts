import client from "./apolloClient";
import {
  CreateOrganizationDocument,
  GetOrganizationDocument,
  GetOrganizationsDocument,
} from "./generated/api.d";

export const getAllOrganizations = async () =>
  client.query({
    query: GetOrganizationsDocument,
  });

export const getOrganization = async (id: string) =>
  client.query({
    query: GetOrganizationDocument,
    variables: { id },
  });

export const createOrganization = async (id: string, name: string) =>
  client.mutate({
    mutation: CreateOrganizationDocument,
    variables: { id, name },
  });
