import * as T from "../server/generated/api.d";

type Props = {
  organization: T.Organization;
};

const Organization = (props: Props) => (
  <tr>
    <td>{props.organization.id}</td>
    <td>{props.organization.name}</td>
  </tr>
);

export default Organization;
