import * as T from "../server/generated/api.d";
import Organization from "./Organization";

type Props = {
  organizations: T.Organization[];
};

const Organizations = (props: Props) => (
  <table>
    <tbody>
      {props.organizations.map((org) => (
        <Organization key={org.id} organization={org} />
      ))}
    </tbody>
  </table>
);

export default Organizations;
