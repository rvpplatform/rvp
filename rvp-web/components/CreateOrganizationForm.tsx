import { useState } from "react";
import { useCreateOrganizationMutation } from "../server/generated/api.d";
import * as T from "../server/generated/api.d";

type Props = {
  formValues?: FormValues;
  addOrganization: (organization: T.Organization) => void;
};

type FormValues = {
  id: string;
  name: string;
};

const CreateOrganizationForm = (props: Props) => {
  const [id, setId] = useState(props.formValues?.id);
  const [name, setName] = useState(props.formValues?.name);
  const [createOrganizationMutation, { data, loading, error }] =
    useCreateOrganizationMutation({
      variables: {
        id: id!!,
        name: name!!,
      },
    });

  const onCreateClicked = async () => {
    const { data } = await createOrganizationMutation();
    if (data) {
      props.addOrganization(data.createOrganization);
    }
  };

  return (
    <div>
      <input
        type="text"
        placeholder="New Organization Name"
        onChange={(e) => setName(e.target.value)}
      />
      <input
        type="text"
        placeholder="New Organization ID"
        onChange={(e) => setId(e.target.value)}
      />
      <span className="mx-2">
        <input type="button" value="Create" onClick={onCreateClicked} />
      </span>
    </div>
  );
};

export default CreateOrganizationForm;
