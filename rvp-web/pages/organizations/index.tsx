import { getAllOrganizations } from "../../server/api";
import * as T from "../../server/generated/api.d";
import Organizations from "../../components/Organizations";
import CreateOrganizationForm from "../../components/CreateOrganizationForm";
import { useState } from "react";

type Props = {
  orgs: T.Organization[];
};

const Index = (props: Props) => {
  const [orgs, setOrgs] = useState(props.orgs);
  const addOrg = (orgs: T.Organization[], org: T.Organization) =>
    setOrgs(orgs.concat(org));

  return (
    <div>
      <CreateOrganizationForm
        addOrganization={(org: T.Organization) => addOrg(orgs, org)}
      />
      <Organizations organizations={orgs} />
    </div>
  );
};

export async function getServerSideProps() {
  const orgs = await getAllOrganizations();
  return {
    props: {
      orgs: orgs.data.organizations,
    },
  };
}

export default Index;
