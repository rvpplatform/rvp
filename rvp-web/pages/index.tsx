import Link from "next/link";

const Index = () => {
  return (
    <ul>
      <li>
        <Link href="/organizations">
          <a>Organizations</a>
        </Link>
      </li>
    </ul>
  );
};
export default Index;
