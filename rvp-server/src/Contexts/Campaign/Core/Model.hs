{-# LANGUAGE DuplicateRecordFields #-}

module Contexts.Campaign.Core.Model where

type CampaignId = String
type CampaignName = String

data Campaign = Campaign
  { _id            :: CampaignId
  , _campaignName  :: CampaignName
  , _volunteers    :: [Volunteer]
  , _targets       :: [Target]
  , _survey        :: Survey
  , _conversations :: [Conversation]
  } deriving (Show, Eq)

type VolunteerId = String
data Volunteer = Volunteer
  { _id   :: VolunteerId
  , _name :: FullName
  } deriving (Show, Eq)

data FullName = FullName
  { _firstName :: String
  , _lastName  :: String
  } deriving (Show, Eq)

type TargetId = String

data Target = Target
  { _id   :: TargetId
  , _name :: FullName
  } deriving (Show, Eq)

type SurveyId = String
type SurveyName = String

data Survey = Survey
  { _id        :: SurveyId
  , _name      :: SurveyName
  , _questions :: [Question]
  } deriving (Show, Eq)

type QuestionId = String
type QuestionPrompt = String

data Question = Question
  { _id     :: QuestionId
  , _prompt :: QuestionPrompt
  } deriving (Show, Eq)

type ConversationId = String

data Conversation = Conversation
  { _id           :: ConversationId
  , _volunteer    :: Volunteer
  , _target       :: Target
  , _surveyResult :: Maybe SurveyResult
  } deriving (Show, Eq)

type SurveyResultId = String
type Answer = String

data SurveyResult = SurveyResult
  { _id       :: SurveyResultId
  , _surveyId :: SurveyId
  , _answers  :: [Answer]
  } deriving (Show, Eq)
