{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Contexts.Organization.Adapters.InMemoryOrgRepoHandler (
  inMemoryOrgRepoHandler
) where

import           Common.Entity                                (Entity (getId))
import           Contexts.Organization.Core.Model             (Organization,
                                                               OrganizationError (OrganizationDoesNotExist),
                                                               OrganizationId)
import           Contexts.Organization.Ports.OrganizationRepo (OrganizationRepo (..))
import           Control.Monad.Freer                          (Eff, reinterpret,
                                                               run)
import           Control.Monad.Freer.State                    (State, get, put,
                                                               runState)
import           Data.List                                    (find)

inMemoryOrgRepoHandler :: Eff '[OrganizationRepo] a -> a
inMemoryOrgRepoHandler request =
  fst (run (runState [] (reinterpret handle request)))
  where
    handle :: OrganizationRepo v -> Eff '[State [Organization]] v
    handle (GetOrg orgId) = do
      (orgs :: [Organization]) <- get
      let org = findOrganizationById orgId orgs
      pure $ toEither (OrganizationDoesNotExist orgId) org

    handle (Save org) = do
      orgs :: [Organization] <- get
      put $ addOrReplace org orgs
      pure (Right org)

    handle GetAllOrgs = do
      orgs :: [Organization] <- get
      pure (Right orgs)

findOrganizationById :: OrganizationId -> [Organization] -> Maybe Organization
findOrganizationById orgId =
  find (\organization -> getId organization == orgId)

addOrReplace :: Eq a => a -> [a] -> [a]
addOrReplace a as = a : filter (/= a) as

toEither :: b -> Maybe a -> Either b a
toEither b Nothing  = Left b
toEither _ (Just a) = Right a
