module Contexts.Organization.Adapters.InMemoryOrgRepoHandlerTest where

import           Contexts.Organization.Adapters.InMemoryOrgRepoHandler (inMemoryOrgRepoHandler)
import           Contexts.Organization.Core.Model                      (Organization (Organization),
                                                                        OrganizationError (OrganizationDoesNotExist))
import           Contexts.Organization.Ports.OrganizationRepo          (getAllOrgs,
                                                                        getOrg,
                                                                        save)
import           Test.Tasty                                            (defaultMain)
import           Test.Tasty.Hspec                                      (Spec,
                                                                        describe,
                                                                        it,
                                                                        shouldBe,
                                                                        testSpec)

main :: IO ()
main = defaultMain =<< testSpec "org repo" spec_prelude

spec_prelude :: Spec
spec_prelude = do

  describe "Org Repo" $ do

    it "returns a given org" $ do
      let result = inMemoryOrgRepoHandler (getOrg "awe")
      let actual = Left $ OrganizationDoesNotExist "awe"
      result `shouldBe` actual

    it "inserts an org" $ do
      let
        org = Organization "123" "abc"
        actual = inMemoryOrgRepoHandler $ do
          save org
          getAllOrgs
        expected = Right [org]

      actual `shouldBe` expected

    it "overwrites a matching org" $ do
      let
        org = Organization "123" "abc"
        actual = inMemoryOrgRepoHandler $ do
          save org
          save org
          getAllOrgs
        expected = Right [org]

      actual `shouldBe` expected
