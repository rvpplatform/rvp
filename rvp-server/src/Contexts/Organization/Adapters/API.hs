{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}

module Contexts.Organization.Adapters.API (routes) where

import qualified Contexts.Organization.Core.Model             as Model
import           Contexts.Organization.Ports.OrganizationRepo (OrganizationRepo)
import qualified Contexts.Organization.Ports.Service          as Service (create,
                                                                          get,
                                                                          getAll)
import           Control.Monad.Freer                          (Eff, LastMember,
                                                               Member)
import           Control.Monad.Trans.Class                    (lift)
import           Data.ByteString.Lazy.Char8                   (ByteString)
import           Data.Morpheus                                (interpreter)
import           Data.Morpheus.Document                       (importGQLDocument)
import           Data.Morpheus.Types                          (RootResolver (..),
                                                               Undefined (..),
                                                               liftEither)
import           Data.Text                                    (Text, pack,
                                                               unpack)
import           Data.Typeable                                (Typeable)
import           Web.Scotty.Trans                             (ScottyError,
                                                               ScottyT, body,
                                                               post, raw)

importGQLDocument "src/Contexts/Organization/Adapters/Schema.gql"

routes
  :: ( ScottyError e
     , Typeable effs
     , Member OrganizationRepo effs
     , LastMember IO effs
     )
  => ScottyT e (Eff effs) ()
routes = do
  post "/graphql" $ body >>= lift . api >>= raw

rootResolver' :: Member OrganizationRepo effs => RootResolver (Eff effs) () Query Mutation Undefined
rootResolver' =
  RootResolver
    { queryResolver = Query {
        organization = organizationResolver,
        organizations = organizationsResolver
    },
      mutationResolver = Mutation {
        createOrganization = createOrganizationResolver
      },
      subscriptionResolver = Undefined
    }
  where
    organizationsResolver = liftEither $ toResponses <$> Service.getAll
    organizationResolver OrganizationArgs { id } = liftEither $ toResponse <$> Service.get (unpack id)
    createOrganizationResolver CreateOrganizationArgs {id, name} =
      liftEither
      $ toResponse
      <$> Service.create (Model.Organization (unpack id) (unpack name))

api :: (Member OrganizationRepo effs, Typeable effs) => ByteString -> Eff effs ByteString
api = interpreter rootResolver'

toResponse
  :: (Applicative m, Show a)
  => Either a Model.Organization -> Either String (Organization m)
toResponse (Right org)  = Right $ toResponse' org
toResponse (Left error) = Left $ show error

toResponse' :: Applicative m => Model.Organization -> Organization m
toResponse' (Model.Organization id name) = Organization { id = pure $ pack id, name = pure $ pack name }

toResponses
  :: (Applicative m, Show a, Functor f)
  => Either a (f Model.Organization)
  -> Either String (f (Organization m))
toResponses (Right orgs) = Right $ toResponse' <$> orgs
toResponses (Left error) = Left $ show error
