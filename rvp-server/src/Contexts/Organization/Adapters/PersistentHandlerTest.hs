module Contexts.Organization.Adapters.PersistentHandlerTest where

import           Contexts.Organization.Adapters.InMemoryOrgRepoHandler (inMemoryOrgRepoHandler)
import           Contexts.Organization.Adapters.PersistentHandler      (organizationDBRef,
                                                                        persistentOrgRepoHandler)
import           Contexts.Organization.Core.Model                      (Organization (Organization),
                                                                        OrganizationError (OrganizationDoesNotExist))
import           Contexts.Organization.Ports.OrganizationRepo          (getAllOrgs,
                                                                        getOrg,
                                                                        save)
import           Test.Tasty                                            (defaultMain)
import           Test.Tasty.Hspec                                      (Spec,
                                                                        describe,
                                                                        it,
                                                                        shouldBe,
                                                                        testSpec)

import           Data.IORef                                            (newIORef)

main :: IO ()
main = defaultMain =<< testSpec "org repo" spec_prelude

spec_prelude :: Spec
spec_prelude = do
  describe "Persistent org repo" $ do
    it "uses the same IO ref" $ do
      db <- organizationDBRef

      persistentOrgRepoHandler db $ do
        save $ Organization "123" "abc"

      result <- persistentOrgRepoHandler db $ do
        save $ Organization "321" "cba"
        getAllOrgs

      result `shouldBe` Right [Organization "321" "cba", Organization "123" "abc"]
