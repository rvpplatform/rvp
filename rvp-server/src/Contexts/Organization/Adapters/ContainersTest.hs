{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}


module Contexts.Organization.Adapters.ContainersTest where
import           Test.Hspec            (Spec, around, describe, hspec, it,
                                        shouldBe)
import           TestContainers.Docker (MonadDocker)
import           TestContainers.Hspec  (MonadDocker, containerRequest, imageTag,
                                        redis, run, setName, withContainers)

containers1
  :: MonadDocker m => m ()
containers1 = do
  -- image <- build $ fromBuildContext "." (Just "Dockerfile")
  -- _ <- run $ containerRequest image
  pure ()


main :: IO ()
main = hspec spec_all


spec_all :: Spec
spec_all = around (withContainers containers1) $
  describe "TestContainers tests" $
    it "test1" $ shouldBe ()
