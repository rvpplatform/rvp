{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Contexts.Organization.Adapters.PersistentHandler where

import           Control.Monad.Freer                          (Eff, interpretM,
                                                               runM)
import           Data.IORef                                   (IORef, newIORef,
                                                               readIORef,
                                                               writeIORef)
import           Data.List                                    (find)

import           Contexts.Organization.Core.Model             (Organization,
                                                               OrganizationError (OrganizationDoesNotExist),
                                                               OrganizationId)

import           Common.Entity                                (Entity (getId))
import           Contexts.Organization.Ports.OrganizationRepo (OrganizationRepo (..))


organizationDBRef :: IO (IORef [Organization])
organizationDBRef = newIORef []

type OrganizationDBRef = IORef [Organization]

persistentOrgRepoHandler
  :: OrganizationDBRef
  -> Eff '[OrganizationRepo, IO] a
  -> IO a
persistentOrgRepoHandler db = runM . interpretM handle
  where
    handle :: OrganizationRepo v -> IO v
    handle (GetOrg orgId) = do
      (orgs :: [Organization]) <- readIORef db
      let org = findOrganizationById orgId orgs
      pure $ toEither (OrganizationDoesNotExist orgId) org

    handle (Save org) = do
      orgs :: [Organization] <- readIORef db
      writeIORef db $ addOrReplace org orgs
      pure (Right org)

    handle GetAllOrgs = do
      orgs :: [Organization] <- readIORef db
      pure (Right orgs)


findOrganizationById :: OrganizationId -> [Organization] -> Maybe Organization
findOrganizationById orgId =
  find (\organization -> getId organization == orgId)

addOrReplace :: Eq a => a -> [a] -> [a]
addOrReplace a as = a : filter (/= a) as

toEither :: b -> Maybe a -> Either b a
toEither b Nothing  = Left b
toEither _ (Just a) = Right a
