{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE GADTs               #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Contexts.Organization.Ports.OrganizationRepo where

import           Contexts.Organization.Core.Model (Organization,
                                                   OrganizationError,
                                                   OrganizationId)
import           Control.Monad.Freer              (Eff, Member, send)
import           Control.Monad.Freer.State        ()


type Error = String

data OrganizationRepo r where
  GetAllOrgs :: OrganizationRepo (Either OrganizationError [Organization])
  GetOrg :: OrganizationId  -> OrganizationRepo (Either OrganizationError Organization)
  Save :: Organization -> OrganizationRepo (Either OrganizationError Organization)

-- Interface for use
getOrg :: Member OrganizationRepo effs => OrganizationId -> Eff effs (Either OrganizationError Organization)
getOrg = send . GetOrg

getAllOrgs :: Member OrganizationRepo effs => Eff effs (Either OrganizationError [Organization])
getAllOrgs = send GetAllOrgs

save :: Member OrganizationRepo effs => Organization -> Eff effs (Either OrganizationError Organization)
save = send . Save
