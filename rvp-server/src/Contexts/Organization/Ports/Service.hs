{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MonoLocalBinds        #-}

module Contexts.Organization.Ports.Service where

import           Contexts.Organization.Core.Model             (Organization,
                                                               OrganizationError,
                                                               OrganizationId)
import           Contexts.Organization.Ports.OrganizationRepo (OrganizationRepo,
                                                               getAllOrgs,
                                                               getOrg, save)
import           Control.Monad.Freer                          (Eff, Member)

-- Service
-- For now these just delegate directly to the repo.
-- In the future we may want to add some business logic here.
create :: Member OrganizationRepo effs =>
  Organization ->
  Eff effs (Either OrganizationError Organization)
create = save

get :: Member OrganizationRepo effs =>
  OrganizationId ->
  Eff effs (Either OrganizationError Organization)
get = getOrg

getAll :: Member OrganizationRepo effs =>
  Eff effs (Either OrganizationError [Organization])
getAll = getAllOrgs
