module Contexts.Organization.Usecases.GetAllTest where

import           Contexts.Organization.Adapters.InMemoryOrgRepoHandler (inMemoryOrgRepoHandler)
import           Contexts.Organization.Core.Model                      (Organization (Organization),
                                                                        OrganizationError (OrganizationDoesNotExist))
import           Contexts.Organization.Ports.OrganizationRepo          (getOrg,
                                                                        save)
import           Contexts.Organization.Ports.Service                   (create,
                                                                        get,
                                                                        getAll)
import           Test.Tasty                                            (defaultMain)
import           Test.Tasty.Hspec                                      (Spec,
                                                                        describe,
                                                                        it,
                                                                        shouldBe,
                                                                        testSpec)

main :: IO ()
main = defaultMain =<< testSpec "organization.usecases.getAll" spec_prelude

spec_prelude :: Spec
spec_prelude = do

  describe "Get Organization" $ do

    it "gets no organizations" $ do
      let
        actual = inMemoryOrgRepoHandler $ do
          getAll
        expected = Right []

      actual `shouldBe` expected

    it "get an organization" $ do
      let
        actual = inMemoryOrgRepoHandler $ do
          create $ Organization "123" "The Firm"
          getAll
        expected = Right [Organization "123" "The Firm"]

      actual `shouldBe` expected

    it "get multiple organizations" $ do
      let
        actual = inMemoryOrgRepoHandler $ do
          create $ Organization "456" "The Group"
          create $ Organization "123" "The Firm"
          getAll
        expected = Right [Organization "123" "The Firm", Organization "456" "The Group"]

      actual `shouldBe` expected
