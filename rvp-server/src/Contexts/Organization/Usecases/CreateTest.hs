module Contexts.Organization.Usecases.CreateTest where

import           Contexts.Organization.Adapters.InMemoryOrgRepoHandler (inMemoryOrgRepoHandler)
import           Contexts.Organization.Core.Model                      (Organization (Organization),
                                                                        OrganizationError (OrganizationDoesNotExist))
import           Contexts.Organization.Ports.OrganizationRepo          (getOrg,
                                                                        save)
import           Contexts.Organization.Ports.Service                   (create,
                                                                        get)
import           Test.Tasty                                            (defaultMain)
import           Test.Tasty.Hspec                                      (Spec,
                                                                        describe,
                                                                        it,
                                                                        shouldBe,
                                                                        testSpec)

main :: IO ()
main = defaultMain =<< testSpec "organization.usecases.create" spec_prelude

spec_prelude :: Spec
spec_prelude = do

  describe "Create Organization" $ do

    it "creates an organization" $ do
      let
        actual = inMemoryOrgRepoHandler $ do
          create $ Organization "123" "The Firm"
        expected = Right $ Organization "123" "The Firm"

      actual `shouldBe` expected

    it "creates and retrieves an organization" $ do
      let
        actual = inMemoryOrgRepoHandler $ do
          create $ Organization "123" "The Firm"
          get "123"
        expected = Right $ Organization "123" "The Firm"

      actual `shouldBe` expected

    it "can't retrieve an organization that doesn't exist" $ do
      let
        actual = inMemoryOrgRepoHandler $ do
          create $ Organization "123" "The Firm"
          get "456"
        expected = Left $ OrganizationDoesNotExist "456"

      actual `shouldBe` expected
