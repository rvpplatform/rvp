{-# LANGUAGE DuplicateRecordFields #-}

module Contexts.Organization.Core.Model where

import           Common.Entity (Entity (..))
import           Prelude       hiding (id)

type ErrorMessage = String
data OrganizationError
  = Unknown ErrorMessage
  | OrganizationDoesNotExist OrganizationId
  deriving (Eq, Show)

type OrganizationId = String

data Organization = Organization {
  id   :: OrganizationId,
  name :: String
} deriving (Show, Eq)

instance Entity Organization where
  getId (Organization id' _) = id'

data Campaign = Campaign {
  id             :: String,
  organizationId :: String,
  name           :: String,
  targets        :: [Target]
}

data Target = Target {
  id      :: String,
  name    :: Name,
  contact :: ContactInfo
}

data Name = Name {
  first :: String,
  last  :: String
}

newtype ContactInfo = ContactInfo {
  phoneNumber :: String
}
