{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE MonoLocalBinds    #-}
{-# LANGUAGE OverloadedStrings #-}

module Lib (
    webServer,
    server,
) where

import qualified Contexts.Organization.Adapters.API               as Organization (routes)
import           Contexts.Organization.Adapters.PersistentHandler (organizationDBRef,
                                                                   persistentOrgRepoHandler)
import           Contexts.Organization.Ports.OrganizationRepo     (OrganizationRepo)
import           Control.Monad.Freer                              (Eff,
                                                                   LastMember,
                                                                   Member)
import           Data.Text.Lazy                                   (Text)
import           Data.Typeable                                    (Typeable)
import           Network.Wai.Handler.Warp                         (defaultSettings,
                                                                   setBeforeMainLoop,
                                                                   setPort)
import           Network.Wai.Middleware.Cors                      (CorsResourcePolicy (corsMethods, corsOrigins, corsRequestHeaders),
                                                                   cors,
                                                                   simpleCorsResourcePolicy,
                                                                   simpleHeaders,
                                                                   simpleMethods)
import           Web.Scotty                                       (Options (Options))
import           Web.Scotty.Trans                                 (ScottyT, get,
                                                                   html,
                                                                   middleware,
                                                                   param,
                                                                   scottyOptsT)

routes
  :: ( Typeable effs
     , Member OrganizationRepo effs
     , LastMember IO effs )
  => ScottyT Text (Eff effs) ()
routes = do
    middleware $ cors (const $ Just apiCors)
    Organization.routes
    otherRoutes

server :: Bool -> IO () -> IO ()
server showStart readyAction = do
    db <- organizationDBRef
    scottyOptsT  (Options showStartMessage settings) (persistentOrgRepoHandler db) routes
  where
    settings =
        setBeforeMainLoop readyAction $
            setPort 8080
            defaultSettings
    showStartMessage = if showStart then 1 else 0

webServer :: IO ()
webServer = server True (pure ())

otherRoutes :: LastMember IO effs => ScottyT Text (Eff effs) ()
otherRoutes = do
    get "/:word" $ do
        beam <- param "word"
        html $ mconcat ["<h1>Scotty, ", beam, " me up!</h1>"]

    -- Providing a helath check endpoint for the ingress to use
    get "/" $ do
        html "Hello World"

apiCors :: CorsResourcePolicy
apiCors = simpleCorsResourcePolicy {
    corsOrigins = Nothing,
    corsMethods = simpleMethods <> ["OPTIONS"],
    corsRequestHeaders = simpleHeaders <> ["Content-Type", "Authorization"]
}
