{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DerivingStrategies    #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE QuasiQuotes           #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeFamilies          #-}

module LibTest where

import           Test.Tasty                       (defaultMain)
import           Test.Tasty.Hspec                 (Spec, around_, describe, it,
                                                   runIO, shouldBe, testSpec)

import           Control.Concurrent               (forkIO, killThread,
                                                   newEmptyMVar, newMVar,
                                                   putMVar, takeMVar)
import           Control.Lens                     ((^.))
import           GHC.Exts                         (fromList)
import           GHC.IO                           (bracket)

import           Data.Aeson                       (Value (..), encode, toJSON)
import           Network.Wreq                     (get, post, responseBody)

import           Lib                              (server)

import           Contexts.Organization.Core.Model
import           Data.Morpheus.Client             (Fetch (fetch),
                                                   defineByDocumentFile, gql)
import           Data.Text                        (Text)

-- This does a bunch of template haskell to generate types for the query
-- In this case it generates a "GetOrgs" data type and constructor.
--
-- Useful to know about in ghci:
-- use :browse LibTest to see everything in scope in this file
-- to get a better idea of what it's creating.  From that we can see
-- it has created:
--
-- type OrganizationsOrganization :: *
--
-- data OrganizationsOrganization
--   = OrganizationsOrganization {LibTest.id :: Text, name :: Text}
--
-- type GetOrgs :: *
--
-- data GetOrgs
--   = GetOrgs {organizations :: [OrganizationsOrganization]}
--
-- You can also use :set --ddump-splices in ghci and :r to see
-- the _exact_ code generated
-- https://github.com/change-metrics/lentille/blob/97975669eeb6584a8a1e3620367d50c4146d25a4/lentille-github/doc/tutorial/graphql-query.md#L96
defineByDocumentFile
  "./src/Contexts/Organization/Adapters/Schema.gql"
  [gql|
      query GetOrgs {
        organizations {
          id
          name
        }
      }
  |]

defineByDocumentFile
  "./src/Contexts/Organization/Adapters/Schema.gql"
  [gql|
      query GetOrg($id: String!) {
        organization(id: $id) {
          id
          name
        }
      }
  |]

defineByDocumentFile
  "./src/Contexts/Organization/Adapters/Schema.gql"
  [gql|
      mutation CreateOrg($id: String!, $name: String!) {
        createOrganization(id: $id, name: $name) {
          id
          name
        }
      }
  |]

main :: IO ()
main = defaultMain =<< testSpec "Lib" spec_lib

spec_lib :: Spec
spec_lib = do
  lock <- runIO $ newMVar True

  let startWeb = do
        takeMVar lock -- wait until the lock is open
        ready <- newEmptyMVar
        threadId <- forkIO $
          server False $ do
            putMVar ready True -- called once the server is ready for requests
        takeMVar ready -- wait until the server is ready
        pure threadId

      stopWeb threadId = do
        killThread threadId
        putMVar lock True -- free the lock for something else to take
      withWeb action = bracket startWeb stopWeb $ const action

  -- why we're using "const action": bracket passes in
  -- whatever's returned from the start fn to the bracketed fn (action),
  -- it being used to acquire and release resources.
  -- so "const action" throws out the passed in threadId

  -- using around_ since we don't need to pass in anything from startWeb
  -- (that being the threadId) to each test
  around_ withWeb $ do
    describe "/" $ do
      it "returns hello world" $ do
        r <- get "http://localhost:8080"
        r ^. responseBody `shouldBe` "Hello World"

    describe "post /graphql" $ do
      it "returns a list of orgs for GetOrgs" $ do
        let
          resolver req = do
            r <- post "http://localhost:8080/graphql" req
            pure $ r ^. responseBody

        -- need to specify the type so that morpheus can figure out
        -- which query we want.  it's args based, but since GetOrgs
        -- has no args (the "()") there's not enough information for it
        res <- (fetch resolver () :: IO (Either String GetOrgs))
        case res of
          Left err   -> fail err
          Right orgs -> orgs `shouldBe` GetOrgs []

      it "create and then fetch an Organization" $ do
        let
          resolver req = do
            r <- post "http://localhost:8080/graphql" req
            pure $ r ^. responseBody

        -- no need to specify since it has enough information to know we mean the
        -- CreateOrg mutation and the GetOrg query
        createRes <- fetch resolver (CreateOrgArgs "123" "The Firm")
        case createRes of
          Left err -> fail err
          Right orgs -> orgs `shouldBe` CreateOrg (CreateOrganizationOrganization "123" "The Firm")

        fetchRes <- fetch resolver (GetOrgArgs "123")
        case fetchRes of
          Left err -> fail err
          Right orgs -> orgs `shouldBe` GetOrg (OrganizationOrganization "123" "The Firm")
