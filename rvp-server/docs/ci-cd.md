# CI/CD

We are leveraging Gitlab significantly to manage our CI/CD. In particular, the top level of this repo has a `.gitlab-cy.yml` file that Gitlab uses to drive a ton of behavior. That file `includes` the one in this project [.gitlab-ci.yml](../.gitlab-ci.yml), which is how Gitlab is able to tell what to do. See [Gitlab's documentation](https://docs.gitlab.com/ee/ci/quick_start/) for a quick overview.

This file defines several stages which together form our pipeline from dev changes --> production. Each stage defines some properties (name, order, which image to run on, and a script to be called to actually run that behavior). We put those scripts in `ci-scripts`.

At time of writing, our stages are test, package, deploy-stage, test-stage, deploy-prod.

## Stages
### Package
Here we take the current version of the app and package it into a docker image. We do some fancy caching with docker (against the :latest tag, which is just the most recently built image), to lower the build time. We store these images in the Gitlab container registry.

### Test
In this step, we pull down the docker image for the commit and run `stack test` on the container. We also do a lot of shenagigans to make sure the container is running in a context with Docker-in-Docker available, and we added docker-cli to the app image so that running test containers works.

### Deploy Stage
Here we take the most recent docker image (from the package step) and push it up to the `stage` namespace in our Google Cloud Kubernetes cluster. That is, we replace whatever is running at the `stage.rvp-api.com` with the current changes.

### Test Stage
Here we run integration / end to end tests against the deployed code in the stage environment. This is currently where we put tests that are too broad in scope to fit in as unit tests in the actual application. If these tests all pass, we promote the changes to production automatically. The tests are currently living in `integration-tests` which is a typescript app leveraging the `jest` test library.

### Deploy Prod
Finally, if everything looks good, we do the same thing we did in deploy stage, except we deploy to the prod namespace aka `prod.rvp-api.com`.

## Notes for the Future
As we start to add stateful dependencies for the application (like a database or third party service integrations), testing starts to become more difficult and we may need to rethink exactly how testing happens. IMO, we will want to break tests down to unit/component level, which will be pure tests living inside the Haskell app which have no dependencies. `Integration` will include dependencies, but we may be able to also include in the haskell app via Test Containers, and `Integrated` tests which are tests that involve other services that we can't package together with the app. Only the latter would need to be run (along with some sanity tests) in the `integration-tests` step (though we will likely want to rename it at that point).