#!/bin/sh
printf '%s' "$1" > key.json # Google Cloud service account key
gcloud auth activate-service-account --key-file key.json

gcloud config set project haskell-hello-world-320123
gcloud config set compute/zone us-central1
gcloud config set container/use_client_certificate False

gcloud container clusters get-credentials haskell-hello-world

kubectl delete secret registry.gitlab.com --ignore-not-found=true
kubectl create secret docker-registry registry.gitlab.com --docker-server=https://registry.gitlab.com --docker-username=$2 --docker-password=$3

curl https://baltocdn.com/helm/signing.asc | apt-key add -
apt-get install apt-transport-https --yes
echo "deb https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list
apt-get update
apt-get install helm

namespace="${4}" 
case "$namespace" in
   "prod") 
    helm upgrade rvp-server ./rvp-server/chart --install --values=./rvp-server/chart/values-prod.yaml --set imageTag="$5" --namespace rvp-server-prod --create-namespace
    ;;
   "stage") 
    helm upgrade rvp-server ./rvp-server/chart --install --values=./rvp-server/chart/values-stage.yaml --set imageTag="$5" --namespace rvp-server-stage --create-namespace
    ;;
   *) 
    echo "Unknown namespace ${4}" 
    ;;
esac