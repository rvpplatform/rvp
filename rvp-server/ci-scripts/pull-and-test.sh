#!/bin/sh
docker login -u gitlab-ci-token -p $1 registry.gitlab.com
docker pull registry.gitlab.com/rvpplatform/rvp:$2

# docker run -i --privileged --entrypoint stack registry.gitlab.com/rvpplatform/rvp:$2 test
docker run -i --rm -v /var/run/docker.sock:/var/run/docker.sock --entrypoint stack registry.gitlab.com/rvpplatform/rvp:$2 test