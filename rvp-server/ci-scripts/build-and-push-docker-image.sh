#!/bin/sh
docker login -u gitlab-ci-token -p $1 registry.gitlab.com
docker pull registry.gitlab.com/rvpplatform/rvp:latest || true
docker build --cache-from registry.gitlab.com/rvpplatform/rvp:latest -t rvp:latest ./rvp-server

docker tag rvp:latest registry.gitlab.com/rvpplatform/rvp:$2
docker tag rvp:latest registry.gitlab.com/rvpplatform/rvp:latest

docker push registry.gitlab.com/rvpplatform/rvp:$2
docker push registry.gitlab.com/rvpplatform/rvp:latest