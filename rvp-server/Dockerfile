FROM haskell:8.10.4

# Install Docker (dependency for using TestContainers in our CI pipeline)
RUN curl -fsSL https://get.docker.com -o get-docker.sh
RUN sh get-docker.sh

# Move to a new directory when building the app. Otherwise we can run into issues like this
# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#understand-build-context
# Where we have a ton of extra weird files which can lead to it breaking the build.
WORKDIR /app
COPY stack.yaml stack.yaml.lock package.yaml ./
# Specify -j1 to avoid a classic OOM error.
RUN stack build -j1 --only-dependencies 

# Actually build the app
COPY . .
RUN stack build
ENTRYPOINT ["stack", "exec", "rvp-server-exe"]
