# rvp-server
RVP Server is a monolithic Haskell application that is deployed on Google Kubernetes Engine. This application is the main place to put all our APIs / logic / persistence assoicated with the RVP project.

## Project Structure

### Core
`app` and `src` are the primary directories where we put actual Haskell code. App is just where we put the `main` function -- i.e. where we wire everything up. Everything else goes into `src`. This project uses [`stack`](https://docs.haskellstack.org/en/stable/README/) and [`cabal`](https://www.haskell.org/cabal/) to manage Haskell dependencies and to build a final executable.

### DevOps
The `chart` directory is where we put our [`Helm chart`](https://helm.sh/) which describes how the application should be deployed into a Kubernetes cluster (in our case, a Google Kubernetes Engine cluster)

### CI/CD
`ci-scripts` and `integration-tests` are part of how we do continuous integration and continuous delivery with this application (along with Gitlab and integrating with Google Cloud)

For more details, look [here](docks/ci-cd.md).

## Dev
We will need to flesh out the dev experience a lot more as we go (especially when we start building out ways to fun locally with k8s/docker and depenendencies like a database). For now:
### Prereqs
Install Haskell, Stack, etc via [this tool](https://www.haskell.org/platform/).

### Dev Flow
`stack build` - will install dependencies locally and build an executable.

`stack test` - will run tests.

`stack exec rvp-server-exe` - run the app locally. Currently this will spin up the `scotty` web server so you can then run `curl localhost:8080/beam` to get back some html with "beam me up" in it!.