import fetch from 'node-fetch';

test('http is disallowed', async () => {
  const result = await fetch('http://stage.rvp-api.com/')
    .then(res => res.status);
  expect(result).toBe(404);
});
