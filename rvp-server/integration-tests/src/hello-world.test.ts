import fetch from 'node-fetch';

test('Scotty, beam me up!', async () => {
  const result = await fetch('https://stage.rvp-api.com/beam')
    .then(res => res.text());
  expect(result).toBe('<h1>Scotty, beam me up!</h1>');
});
