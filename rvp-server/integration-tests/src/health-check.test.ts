import fetch from 'node-fetch';

test('200 on health check', async () => {
  const result = await fetch('https://stage.rvp-api.com/')
    .then(res => res.status);
  expect(result).toBe(200);
});
